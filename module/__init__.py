def alg():
    x = {1: 1}
    delta = 1
    e1 = 0.003
    e2 = 0.03

    x[2] = x[1] + delta
    f1 = func(x[1])
    f2 = func(x[2])

    if f1 > f2:
       x[3] = x[1] + 2 * delta
    else:
       x[3] = x[1] - delta

    f3 = func(x[3])

    fs = [f1, f2, f3]
    fMin = min(fs)
    fMinIndex = -1
    for i in range(0, fs.count() - 1):
        if fMin == fs[i]:
            fMinIndex = i
            break

    up = (((x[2] ** 2) - (x[3] ** 2)) * f1) + (((x[3] ** 2) - (x[1] ** 2)) * f2) + (((x[1] ** 2) - (x[2] ** 2)) * f3)
    down = ((x[2] - x[3]) * f1) + ((x[3] - x[1]) * f2) + ((x[1] - x[2]) * f3)

    if down == 0:
        x[1] = fMinIndex
        # Перейти к шагу 2
    else:
        # Шаг 8

    x = 0.5 * (up/down)
    funcX = func(x)

    condition1 = ((fMin - funcX)/funcX) < e1
    condition2 = ((fMinIndex - x)/x) < e2

    if condition1 & condition2:
        result = x
    elif condition1 | condition2:
        print(condition1 | condition2)

def func(x):
    return 2 * (x ** 2) + (16 / x)

alg()